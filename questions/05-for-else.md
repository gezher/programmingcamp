# 实现类似 Python 中的 for-else 语法

~~~
for(array || object, function(i, el, array, break) {
  // do sth
  // 调用 break 方法时不再执行此方法
}).else((i, array) => {
  // 在 for 中方法没执行过的情况下，会执行此方法体中的内容
  // i: 最后一个 index
});
~~~