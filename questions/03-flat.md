# 实现 flat 函数，在不改变原数组的情况下将多维数组平铺

~~~
const array = ['a', 'b', ['ba', 'bb', 'bc'], 'c', ['ca', 'cb', ['cba', 'cbb']]]

flat(array)   // ['a', 'b', 'ba', 'bb', 'bc', 'c', 'ca', 'cb', 'cba', 'cbb']
~~~