# 实现一个 overloadCompose(functions) 实现方法重载

- functions: 不同函数的数组

- SameArgumentsException: 有两个函数的参数个数相同

- NotAllowArrowFunctionException: 参数中不允许存在箭头函数

e.g.

~~~
function square(a) {
  return Math.pow(a, 2);
}

function rectangle(l, w) {
  return l * w;
}

function cube(l, w, h) {
  return l * w * h;
}

const area = overloadCompose([square, rectangle, cube]);

area(3)
area(2, 4)
area(2, 5, 8)

/*
Output

9
8
80

*/
~~~
