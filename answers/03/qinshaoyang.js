function flat(array) {
  const result = [];

  const find = (arr) => {
    for (let item of arr) {
      const isArray = Array.isArray(item);
      if (isArray) {
        find(item);
      } else {
        result.push(item);
      }
    }
  }

  find(array);

  return result;
}

const array = ['a', 'b', ['ba', ['cc', ['ee', 'ff'], 'dd', 'aa'], 'bb', 'bc'], 'c', ['ca', 'cb', ['cba', 'cbb']]];

// test
console.log( flat(array) );
