const array = ['a', 'b', ['ba', 'bb', 'bc'], 'c', ['ca', 'cb', ['cba', 'cbb']]]

const flat = (array) => array.reduce((sum, e) => e instanceof Array ? [...sum, ...flat(e)] : [...sum, e], []);

console.log(flat(array))   // ['a', 'b', 'ba', 'bb', 'bc', 'c', 'ca', 'cb', 'cba', 'cbb']