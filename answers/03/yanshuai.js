const array = ['a', 'b', ['ba', 'bb', 'bc'], 'c', ['ca', 'cb', ['cba', 'cbb']]]
function flat(array) {
    return array.join(",").split(",");
}
console.log(flat(array));