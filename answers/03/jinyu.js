const array = ['a', 'b', ['ba', 'bb', 'bc'], 'c', ['ca', 'cb', ['cba', 'cbb']]];

const flatten = array => {
  const result = [];
  let stack = [];
  stack = stack.concat(array);
  while (stack.length) {
    const element = stack.shift();
    if (element instanceof Array) {
      stack = stack.concat(element);
    } else {
      result.push(element);
    }
  }
  return result;
};

console.log(flatten(array));
