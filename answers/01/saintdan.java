import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author <a href="http://github.com/saintdan">Liao Yifan</a>
 * @date 9/9/16
 * @since JDK1.8
 */
public class Range {

  @SuppressWarnings("unchecked")
  public static List range(final int start, final int stop, final int step) {
    List list = new ArrayList();
    rangeIterable(start, stop, step).forEach(list::add);
    return list;
  }

  public static Iterable rangeIterable(final int start, final int stop, final int step) {
    if (step <= 0) {
      throw new IllegalArgumentException("step > 0 is required!");
    }
    return () -> new Iterator() {
      private int counter = start;

      public boolean hasNext() {
        return counter <= stop;
      }

      public Integer next() {
        try {
          return counter;
        } finally {
          counter += step;
        }
      }

      public void remove() {
      }
    };
  }

  public static List range(final int start, final int stop) {
    return range(start, stop, 1);
  }

  public static List range(final int stop) {
    return range(0, stop, 1);
  }
}
