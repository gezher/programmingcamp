const range = (x, y, z) => {
  let i;
  let r;
  let s;
  const array = []
  
  if(!y) {
    i = 0;
    r = x;
  } else {
    i = x;
    r = y;
  }
  s = z || 1;
  for(; i <= r; i += s) {
    array.push(i);
  }
  return array;
}

console.log(range(5));
console.log(range(2, 5));
console.log(range(2, 5, 2));