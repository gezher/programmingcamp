// range([start=0], end, [step=1])
function range(start, end, step) {
  const argLen = arguments.length;

  if (argLen === 0) {
    throw new Error('require at least one parameter');
  }

  for (let i = 0; i < argLen; i++) {
    if (typeof arguments[i] !== 'number') {
      throw new Error('all arguments must be number');
    }
  }

  if (argLen === 1) {
    end = start;
    start = 0;
  }

  if (start > end) {
    start = end;
  }
  step = step || 1;

  const result = [];
  for (let i = start; i <= end; i += step) {
    result.push(i);
  }

  return result;
}

// test
console.log( range(5) );
console.log( range(3, 5) );
console.log( range(2, 10, 2) );
