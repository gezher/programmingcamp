const range = (start, end, step) => {
  if(!end) {
    end = start;
    start = 0;
  }
  start = start || 0;
  step = step || 1;
  var arr = [];
  for(var i = start; i <= end; i += step) {
    arr.push(i);
  }
  return arr;
}
console.log(range(5));
console.log(range(3, 5));
console.log(range(2, 10, 2));

