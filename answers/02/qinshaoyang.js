function square(a) {
  return Math.pow(a, 2);
}

function rectangle(l, w) {
  return l * w;
}

function cube(l, w, h) {
  return l * w * h;
}

function overloadCompose(functions) {
  const unique = new Set();
  const reorderFunctions = [];
  for (let func of functions) {
    const argNum = func.length;
    if (unique.has(argNum)) {
      throw new Error(`arguments count repeat at function: ${func.name}`)
    }
    unique.add(argNum);

    reorderFunctions[argNum] = func;
  }

  return function(...args) {
    const argLen = args.length;
    return reorderFunctions[argLen](...args);
  }
}

// test
const area = overloadCompose([square, rectangle, cube]);
console.log( area(3) );
console.log( area(2, 4) );
console.log( area(2, 5, 8) );
