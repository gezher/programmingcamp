const overloadCompose = function(funcs) {
  const sortedFuncs = funcs.sort((a, b) => a.length > b.length);
  sortedFuncs.reduce((sum, func) => {
    if(sum === func.length) {
      throw new Exception('SameArgumentsException');
    }
    try {
      if((typeof func).toLowerCase() === 'function' && func.arguments) {
        throw new Exception('Arguments must be functions');
      }
    } catch (e) {
      throw new Exception('NotAllowArrowFunctionException');
    }
    return 0 + func.length;
  }, 0);
  return function () {
    return sortedFuncs[arguments.length - 1](...arguments);
  };
};

function square(a) {
  return Math.pow(a, 2);
}

function rectangle(l, w) {
  return l * w;
}

function cube(l, w, h) {
  return l * w * h;
}

const area = overloadCompose([square, rectangle, cube]);

console.log(area(3));
console.log(area(2, 4));
console.log(area(2, 5, 8));