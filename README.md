# 编程训练营

一个训练编程思想（不是技能）的项目。

## 如何参加训练

1. 克隆项目: `git clone git@bitbucket.org:gezher/programmingcamp.git`
2. 检出你自己的分支: `git checkout -b zhangrui`
3. 在 questions 目录下找到题目
4. 在 answers 目录对应位置写好你的代码，文件命名规则: answers/<number of periods>/<your id>.[js|java|etc.] `answers/01/zhangrui.js`
5. 提交并给 master 分支发 PR

## 如何投稿题目

- 在 questions 目录中写好题目，然后发 RP

## 成为教练

1. 申请成为某一期的教练
1. 把题目写到白板上
1. 完成这一期的 CodeReview
1. 如果有必要，可以对题目和提交的代码进行讲解

## 题目
- [01-range](questions/01-range.md)
- [02-functionOverload](questions/02-functionOverload.md)
- [03-flat](questions/03-flat.md)